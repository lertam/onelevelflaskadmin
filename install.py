# -*- coding:utf-8 -*-
from core import app, db, user_datastore
from core.models import FreeSpace

db.drop_all()
db.create_all()

with app.app_context():
    member_role = user_datastore.create_role(name = 'member')
    junrole = user_datastore.create_role(name = 'junadmin')
    user_datastore.commit()
    admin_role = user_datastore.create_role(name = 'admin')
    user_datastore.commit()
    admin_user = user_datastore.create_user(action = 'install', email = app.config['ADMIN_EMAIL'])
    user_datastore.activate_user(admin_user)
    user_datastore.commit()
    fs = FreeSpace(user = admin_user)
    db.session.add(fs)
    db.session.commit()
    print('Готово!')

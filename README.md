Кратко:
* One Level System using Flask Admin.
* Версия 1.2.3

Настройка:

1. Заполните файл config-sample.py нужными значениями.
2. Переименуйте файл config-sample.py в config.py
3. Убедитесь, что у вас установлены все необходимые пакеты и существует база данных.
4. Запустите скрипт install.py.
5. На e-mail администратора придет письмо с паролем.

Требования:
* Python 3.5
* Flask 
* Flask_Admin
* Flask_Security
* Flask_SQLAlchemy
* Flask_Babelex

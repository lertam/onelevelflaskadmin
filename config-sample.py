SECRET_KEY = 'secret_key'

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://<db_username>:<db_password>@<db_host>/<db_name>?charset=utf8'
#SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
SQLALCHEMY_ECHO = False

SECURITY_URL_PREFIX = '/admin'
SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
SECURITY_PASSWORD_SALT = 'ATGUOHAELKiubahiughaerGOJAEGj'

SECURITY_LOGIN_URL = '/login/'
SECURITY_LOGOUT_URL = '/logout/'
SECURITY_REGISTER_URL = '/register/'
SECURITY_FLASH_MESSAGES = False

SECURITY_REGISTERABLE = True
SECURITY_RECOVERABLE = False
SECURITY_SEND_REGISTER_EMAIL = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Настройки почтового аккаунта
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = '<e-mail username>'
MAIL_PASSWORD = '<e-mail password>'
MAIL_DEFAULT_SENDER = 'Cash Step'

# Настройки для генерации паролей
PASSWORD_HARD = False # Использование в паролях спец.символов( !*@ и т.д.)
PASSWORD_LENGTH = 8 # Длина пароля

# E-mail администратора (исподьзуется при установке)
ADMIN_EMAIL = '<Administrator e-mail address'

BABEL_DEFAULT_LOCALE = 'ru'

SERVER_NAME = 'localhost:5000' #Change to your server name.

# -*- coding:utf-8 -*-
from core import app, db
from flask_security import current_user
from flask_wtf import FlaskForm
from wtforms import TextField, HiddenField, DecimalField, SelectField, StringField, SelectMultipleField
from flask_admin.form.fields import Select2TagsField, Select2Field
from wtforms.validators import Required, Email
from core.models import Role
from core.functions import MSG_INVALID_EMAIL, MSG_REQUIRED

class UserForm(FlaskForm):
	"""
		Форма для отображения в личном кабинете пользоателя
	"""
	account = TextField(u'', validators = [Required(message = MSG_REQUIRED)])



role_choices = [
	('member', u'Участник'),
	('junadmin', u'Сотрудник'),
]

class CreateForm(FlaskForm):
	email = StringField(label = 'E-mail', validators = [Required(message = MSG_REQUIRED), Email(message = MSG_INVALID_EMAIL)])
	last_name = StringField(label = u'Фамилия', validators = [Required(message = MSG_REQUIRED)])
	first_name = StringField(label = u'Имя', validators = [Required(message = MSG_REQUIRED)])
	middle_name = StringField(label = u'Отчество')
	money = DecimalField(label = u'Сумма', default = 0)
	role = SelectField(label = u'Роль', choices = role_choices)

	def populate_obj(self, obj):
		obj.first_name = self.first_name.data
		obj.middle_name = self.middle_name.data
		obj.last_name = self.last_name.data
		obj.money = self.money.data
		obj.email = self.email.data
		role = Role.query.filter(Role.name == self.role.data).first()
		obj.roles = [role, ]


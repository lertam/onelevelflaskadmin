# -*- coding:utf-8 -*-
from flask_security import current_user
from flask_security.datastore import SQLAlchemyDatastore, UserDatastore
from flask_security.utils import encrypt_password, get_identity_attributes
from flask import render_template, flash
from flask_mail import Message
from core import mail, app, db
from core.models import Role, User, FreeSpace, Transaction
from flask_mail import Message
from sqlalchemy import func
from wtforms.validators import ValidationError
from random import choice, randint

# Messages Const
MSG_INVALID_EMAIL = u'Неправильный email'
MSG_REQUIRED = u'Это поле необходимо заполнить'
MSG_CURATOR_FULL = u'Вы можете создать только по 3 пользователя для каждого куратора.'

# Roles Ids
ROLE_ADMIN = 3
ROLE_MEMBER = 1
ROLE_JUNADMIN = 2

class SQLAlchemyUserDatastore(SQLAlchemyDatastore, UserDatastore):
    """A SQLAlchemy datastore implementation for Flask-Security that assumes the
    use of the Flask-SQLAlchemy extension.
    """
    def __init__(self, db, user_model, role_model):
        SQLAlchemyDatastore.__init__(self, db)
        UserDatastore.__init__(self, user_model, role_model)

    def get_user(self, identifier):
        if self._is_numeric(identifier):
            return self.user_model.query.get(identifier)
        for attr in get_identity_attributes():
            query = getattr(self.user_model, attr).ilike(identifier)
            rv = self.user_model.query.filter(query).first()
            if rv is not None:
                return rv

    def _is_numeric(self, value):
        try:
            int(value)
        except (TypeError, ValueError):
            return False
        return True

    def find_user(self, **kwargs):
        return self.user_model.query.filter_by(**kwargs).first()

    def find_role(self, role):
        return self.role_model.query.filter_by(name=role).first()

    def create_user(self, action, **kwargs):
        """Creates and returns a new user from the given parameters."""
        kwargs = self._prepare_create_user_args(**kwargs)
        user = self.user_model(**kwargs)
        password = generatePassword()
        user.password = encrypt_password(password)
        with app.app_context():
            if action == 'install':
                #Admin created (System installed)
                role = self.find_role('admin')
                user.roles = [role, ]
                msg = Message('Cash Step', recipients = [user.email,])
                msg.html = render_template('email/admin/installed.html', user = user, password = password)
                mail.send(msg)
            elif action == 'create':
                msg = Message('Cash Step!', recipients = [user.email,])
                msg.html = render_template('email/member/registered.html', user = user, password = password)
                mail.send(msg)
        return self.put(user)


values = ['abcdefghijklmnopqrstuvwxyz', '0123456789', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ']
keys = ['small', 'nums', 'big']

if app.config['PASSWORD_HARD']:
    keys.append('special')
    values.append("^!\$%&/()=?{[]}+~#-_.:,;<>|\\")

char_set = {keys[i]: values[i] for i in range(0,3)}

def generatePassword(length=app.config['PASSWORD_LENGTH']):

    password = []

    while len(password) < length:
        key = choice(keys)
        a_char = char_set[key][randint(0,len(char_set[key]) - 1)]
        if check_prev_char(password, char_set[key]):
            continue
        else:
            password.append(a_char)
    return ''.join(password)

def check_prev_char(password, current_char_set):

    index = len(password)
    if index == 0:
        return False
    else:
        prev_char = password[index - 1]
        if prev_char in current_char_set:
            return True
        else:
            return False

@app.context_processor
def my_utility_processor():

    def getFullName(user):
        res = ""
        if user.last_name is not None:
            res = res  + ' ' + user.last_name;
        if user.first_name is not None:
            res = res + ' ' + user.first_name
        if user.middle_name is not None:
            res = res + ' ' + user.middle_name
        return res

    return dict(getFullName = getFullName)


def getPercents(sum, type):
    if type == 'curator':
        return sum / 2
    elif type == 'admin':
        return sum / 4
    elif type == 'owner':
        return sum / 4
    else:
        return False


def overview_function():
    member_role = Role.query.filter(Role.name == 'member').first()
    members = db.session.query(func.count('*')).filter(User.roles.contains(member_role)).first()[0]
    info = dict(
        members = members,
        withdraw_sum = db.session.query(func.sum(Transaction.sum)).filter(Transaction.status == u"Закрыто").filter(Transaction.type == u'Пользователю').first()[0] or 0,
        admin_email = app.config['ADMIN_EMAIL'],
    )
    return info

app.jinja_env.globals.update(overview = overview_function)


def checkStatus(user):
    user.passive = True if (user.money >= 7000) else False
    #db.session.add(user)


def getPosition():
    FreeSpaceCheck()
    fs = db.session.query(FreeSpace).order_by(FreeSpace.id).first()
    fs.count = fs.count - 1
    db.session.add(fs)
    db.session.commit()
    return fs.user

def FreeSpaceCheck(from_delete = False):
    filled = FreeSpace.query.filter(FreeSpace.count == 0).all()
    if len(filled) != 0:
        for fs in filled:
            fs.user.full = True
            if not from_delete:
                msg = Message('Cash Step', recipients = [fs.user.email,])
                msg.html = render_template('email/member/filled.html')
                mail.send(msg)

            refs = User.query.filter(User.curator == fs.user).order_by(User.id).all()

            for ref in refs:
                ref.curator = None
                db.session.add(FreeSpace(user = ref))
            db.session.delete(fs)
            db.session.commit()

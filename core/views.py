# -*- coding:utf-8 -*-
from core import app, db
from flask import render_template, redirect, url_for, jsonify, request
from flask_security import login_required, current_user, roles_accepted
from core.forms import UserForm
from core.models import User, FreeSpace, WithdrawTicket
from core.functions import getPosition, FreeSpaceCheck


@app.route('/', methods = ['POST', 'GET'])
@login_required
@roles_accepted('member', 'admin', 'junadmin')
def index():
	if current_user.has_role('admin') or current_user.has_role('junadmin'):
		return redirect(url_for('admin.index'))
	referals = User.query.filter_by(curator_id = current_user.id).all()
	ticket = None
	if len(WithdrawTicket.query.filter(WithdrawTicket.user == current_user).filter(WithdrawTicket.status != u'Закрыто').all()) > 0:
		ticket = WithdrawTicket.query.filter(WithdrawTicket.user == current_user).filter(WithdrawTicket.status != u'Закрыто').first()
	form = UserForm()
	if form.validate_on_submit():
		if 'rp' in request.values:
			diff = current_user.money - 7000
			current_user.curator = getPosition()
			current_user.full = False
			current_user.money = current_user.money - 7000
			current_user.passive = False
			db.session.add(current_user)
			if diff > 0:
				tran = WithdrawTicket(user = current_user, sum = diff, account = form.account.data)
				db.session.add(tran)
			db.session.commit()
			return redirect(url_for('index'))
		elif 'wd' in request.values:
			tran = WithdrawTicket(user = current_user, sum = current_user.money, account = form.account.data)
			db.session.add(tran)
			db.session.commit()
			return redirect(url_for('index'))

	queue = pos()
	return render_template('index.html', form = form, referals = referals, ticket = ticket, queue=queue)

def pos():
	if current_user.has_role('admin') or current_user.has_role('junadmin'):
		return 0
	userfs = FreeSpace.query.filter(FreeSpace.user == current_user).first()
	if userfs:
		fps = FreeSpace.query.filter(FreeSpace.id <= userfs.id).all()
		sp = 0
		for fp in fps:
			sp += fp.count
		return sp
	else:
		if current_user.full:
			return 0
		else:
			fss = FreeSpace.query.all()
			sp=0
			for fs in fss:
				sp += fs.count
			sp += 3
			return sp

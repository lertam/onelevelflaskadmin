from flask import Flask, request, abort, url_for, g
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, login_required, current_user
from flask_admin import Admin
from flask_mail import Mail
from flask_babelex import Babel
global user
app = Flask(__name__)
app.config.from_pyfile('../config.py')

db = SQLAlchemy(app)

babel = Babel(app, default_locale = 'ru')
mail = Mail(app)


from core.functions import *
from core.models import User, Role

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

admin = Admin(
	app,
	'Cash Step',
	base_template = 'my_master.html',
	template_mode = 'bootstrap3',
)

from core.views import *
from core.admin_views import *

# -*- coding:utf-8 -*-
from core import db
from flask_security import UserMixin, RoleMixin
import datetime

roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)

class Role(db.Model, RoleMixin):
	id = db.Column(db.Integer(), primary_key=True)
	name = db.Column(db.String(80), unique=True)
	description = db.Column(db.String(255))

	def __str__(self):
		return self.name

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    middle_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    passive = db.Column(db.Boolean(), default = False)
    full = db.Column(db.Boolean(), default = False)
    confirmed_at = db.Column(db.DateTime(), default = datetime.datetime.utcnow)
    money = db.Column(db.Float(), default = 0)
    curator_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    curator = db.relationship('User', remote_side=[id])
    roles = db.relationship('Role', secondary=roles_users)

    def __str__(self):
        return self.email

class WithdrawTicket(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref = db.backref('withdrawtickets'))
    sum = db.Column(db.Float(), default = 0)
    account = db.Column(db.String(300))
    status = db.Column(db.Enum(u'Ожидает', u'Закрыто'), default = u'Ожидает')
    created = db.Column(db.DateTime(), default = datetime.datetime.utcnow)

class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    from_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    from_user = db.relationship('User', foreign_keys=[from_user_id], backref = db.backref('transactions'))#, lazy = 'dynamic'))
    to_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    to_user = db.relationship('User', foreign_keys=[to_user_id])
    type = db.Column(db.Enum(u'Куратору',u'Администратору',u'Владельцу', u'Пользователю'))
    sum = db.Column(db.Float(), default = 0)
    status = db.Column(db.Enum(u'Ожидает', u'Закрыто'), default = u'Ожидает')
    created = db.Column(db.DateTime(), default = datetime.datetime.utcnow)
    ex = db.Column(db.String(300))

class FreeSpace(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref = db.backref('freespaces', lazy = 'dynamic'))
    count = db.Column(db.Integer, default = 3)
    created = db.Column(db.DateTime(), default = datetime.datetime.utcnow)

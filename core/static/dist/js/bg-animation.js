canvas = document.getElementById("bg-animation");
h = $("body").height()-5;
w = $("body").width();
$(canvas).attr({"width":w+"px","height":h+"px"});
ctx = canvas.getContext("2d");
last = {};

x_step = 7/2;
y_step = 1;

speed = 0.05;

ratio = h/w;

interval_pointer = 0;


function startDrawing(interval_s){
	ctx.fillStyle = "#151515";
	ctx.fillRect(0,0,w,h);
	last = {x: 0, y: h};
	drawGrid();
	ctx.beginPath();
	ctx.moveTo(0,h);
	interval_pointer = setInterval(drawStep, interval_s*1000);
}

function endDrawing(){
	clearInterval(interval_pointer);
};

function drawGrid(){
	ctx.beginPath();
	ctx.moveTo(0,0);
	ctx.lineWidth = 0.05;
	ctx.strokeStyle = "rgba(255,235,59,0.5)";
	
	//Horizontal
	offset_y = 0;
	step = h/15;
	for(i=0;i!=15;i++){
		offset_y += step;
		ctx.moveTo(0,offset_y);
		ctx.lineTo(w, offset_y);
		ctx.stroke();
	};
	//Vertical
	offset_x = 0;
	step = w/6;
	for(i=0;i!=6;i++){
		offset_x += step;
		ctx.moveTo(offset_x,0);
		ctx.lineTo(offset_x,h);
		ctx.stroke();
	};

	ctx.closePath();
};

function drawStep(){
	ctx.strokeStyle = "#ff9800";
	ctx.lineWidth = 1;

	max = h/2;
	min = -1*max;
	
	if(last.y >= h*0.8){
		max = max/3;
		min = 0;
	};

	direction = Math.random()*(max - min) + min;
	y_step = direction;

	last.x += x_step;
	last.y -= y_step * ratio / 4;

	//fillUnderGraphZone();
	ctx.lineTo(last.x, last.y);
	ctx.stroke();

	if(last.x >= w) refreshDrawing();
	if(last.y <= -1*h) refreshDrawing();
};

function fillUnderGraphZone(){
	ctx.fillStyle = "rgba(255,152,0,0.01)";
	ctx.fillRect(last.x-3, last.y+2, 5, h-last.y);
};

function refreshDrawing(){
	endDrawing();
	startDrawing(speed);
};

$(document).ready(function(){
	startDrawing(speed);
});
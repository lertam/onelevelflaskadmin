# -*- coding: utf-8 -*-
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from flask_admin.actions import action
from flask_security import current_user
from sqlalchemy import func, select
from wtforms.validators import email, required
from core import admin, security, db, user_datastore, mail
from core.models import User, Transaction, WithdrawTicket
from flask import redirect, url_for
from core.functions import *
from flask import url_for, render_template, redirect, request, abort, flash
from flask_admin.model.template import LinkRowAction, EndpointLinkRowAction
from flask_admin.form.fields import Select2TagsField
from wtforms.validators import DataRequired, Email
import os.path

from flask_mail import Message

#import decimal

from flask_babelex import gettext, lazy_gettext, refresh

def role_format(roles):
		for role in roles:
			name = role.name
			if name == 'member':
				return u'Участник'
			elif name == 'junadmin':
				return u'Сотрудник'
			else:
				return u'Администратор'

def sum_check(form, field):
	usr = User.query.get(form._obj.id)
	if usr.money != field.data and (not (field.data > 0)):
		raise ValidationError(u'Сумма должна быть > 0')

# Panel Views For admin

class MemberAdminView(sqla.ModelView):
	column_list = ('last_name', 'first_name', 'middle_name', 'email', 'money', 'passive', 'confirmed_at', 'curator', 'full')
	column_searchable_list = ['first_name', 'email', 'last_name']
	column_filters = ['passive']
	column_labels = dict(
		first_name=u'Имя',
		middle_name=u'Отчество',
		last_name=u'Фамилия',
		curator=u'Куратор',
		money=u'Средства',
		passive=u'Пассивный',
		confirmed_at=u'Создан',
		full = u'Заполнен',
		roles = u'Роль',
	)
	column_formatters = dict(
		money = lambda v, c, m, p: (str)(m.money)+u' руб.',
		roles = lambda v, c, m, p: role_format(m.roles),
	)
	form_columns = ('email', 'last_name', 'first_name', 'middle_name', 'money')
	form_args = dict(
		last_name=dict(label=gettext(u'Фамилия'), validators=[DataRequired(message = MSG_REQUIRED)]),
        first_name=dict(label=gettext(u'Имя'), validators=[DataRequired(message = MSG_REQUIRED)]),
        middle_name=dict(label=gettext(u'Отчество')),
        email=dict(label=u'Email', validators=[DataRequired(message = MSG_REQUIRED), Email(message = MSG_INVALID_EMAIL)]),
        money=dict(label=u'Средства', validators = [sum_check]),
	)
	edit_modal = True
	create_modal = True
	can_set_page_size = True
	can_export = True
	can_delete = True

	def is_accessible(self):
		if not current_user.is_authenticated : return False
		if current_user.has_role('admin'): return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('login', next=request.url))

	def get_query(self):
		member_role = Role.query.get(ROLE_MEMBER)
		return (self.session.query(User).filter(User.roles.contains(member_role)))

	def get_count_query(self):
		member_role = Role.query.get(ROLE_MEMBER)
		return (self.session.query(func.count('*')).filter(User.roles.contains(member_role)))

	def create_model(self, form):
		try:
			model = self.model()
			form.populate_obj(model)
			member_role = Role.query.get(ROLE_MEMBER)
			user = user_datastore.create_user(action = 'create', email = model.email,
		 		first_name = model.first_name,
		 		middle_name = model.middle_name,
		 		last_name = model.last_name,
		 		curator = getPosition(),
		 		roles = [member_role, ],
		 		money = 0,
		 		passive = False,
		 		active = True,
		 	)
			self._on_model_change(form, model, True)
			total = model.money or 0
			if total > 0:
			 	tran1 = Transaction(from_user = user, to_user = user.curator, type=u'Куратору', sum = getPercents(sum = total, type = 'curator'))
			 	tran2 = Transaction(from_user = user, type=u'Администратору', sum = getPercents(sum = total, type='admin'))
			 	tran3 = Transaction(from_user = user, type=u'Владельцу', sum = getPercents(sum=total, type='owner'))
			 	db.session.add(tran1)
			 	db.session.add(tran2)
			 	db.session.add(tran3)
			db.session.commit()
			FreeSpaceCheck()
		except Exception as ex:
			if not self.handle_view_exception(ex):
				flash(u'Невозможно создать запись: %s' % str(ex), 'mycat')
			self.session.rollback()
			return False
		return model

	def update_model(self, form, model):
	 	try:
	 		self._on_model_change(form, model, False)
	 		newEmail = None
		 	if form.data['email'] != model.email: newEmail = form.data['email']
		 	form.populate_obj(model)
		 	checkStatus(model)
		 	if newEmail is not None:
		 		password = generatePassword()
		 		model.password = encrypt_password(password)
		 		msg = Message('Cash Step', recipients = [newEmail,])
		 		msg.html = render_template('email/member/registered.html', user = model, password = password)
		 		mail.send(msg)
	 		self.session.commit()
	 	except Exception as ex:
	 		if not self.handle_view_exception(ex):
	 			flash(u'Невозможно редактировать запись: %s' % str(ex), 'mycat')
	 		self.session.rollback()
	 		return False
	 	else:
	 		self.after_model_change(form, model, False)
	 	return True

	def _on_model_change(self, form, model, is_created):
		if form.data['money'] > model.money and not is_created:
			diff = float(form.data['money']) - float(model.money)
			tran1 = Transaction(from_user = model, to_user = user.curator, type=u'Куратору', sum = getPercents(sum = diff, type = 'curator'))
			tran2 = Transaction(from_user = model, type=u'Администратору', sum = getPercents(sum = diff, type='admin'))
			tran3 = Transaction(from_user = model, type=u'Владельцу', sum = getPercents(sum = diff, type='owner'))
			db.session.add(tran1)
			db.session.add(tran2)
			db.session.add(tran3)
		elif not is_created and form.data['money'] < model.money:
			tran1 = Transaction.query.filter(Transaction.from_user == model).filter(Transaction.type == u'Куратору').filter(Transaction.status == u'Ожидает').order_by(Transaction.id.desc()).first()
			tran2 = Transaction.query.filter(Transaction.from_user == model).filter(Transaction.type == u'Администратору').filter(Transaction.status == u'Ожидает').order_by(Transaction.id.desc()).first()
			tran3 = Transaction.query.filter(Transaction.from_user == model).filter(Transaction.type == u'Владельцу').filter(Transaction.status == u'Ожидает').order_by(Transaction.id.desc()).first()
			tran1.sum = getPercents(sum = form.data['money'], type = 'curator')
			tran2.sum = getPercents(sum = form.data['money'], type='admin')
			tran3.sum = getPercents(sum = form.data['money'], type='owner')
		


	def delete_model(self, model):
		try:
			if model.curator:
				fs = FreeSpace.query.filter(FreeSpace.user == model.curator).first()
				if fs:
					fs.count = fs.count + 1
			fs = FreeSpace.query.filter(FreeSpace.user == model).first()
			if fs is not None:
				fs.count = 0
				FreeSpaceCheck(from_delete = True)
			trans = Transaction.query.filter(Transaction.from_user == model).all()
			for tran in trans: self.session.delete(tran)
			self.on_model_delete(model)
			self.session.flush()
			self.session.delete(model)
			self.session.commit()
		except Exception as ex:
			if not self.handle_view_exception(ex):
				flash(gettext(u'Невозможно удалить пользователя. %(error)s', error=str(ex)), 'error')
			self.session.rollback()
			return False
		else:
			self.after_model_delete(model)
		return True

class EmployerAdminView(sqla.ModelView):
	column_list = ('last_name', 'first_name', 'middle_name', 'email',)
	column_searchable_list = ['first_name', 'email', 'last_name']
	column_labels = dict(
		first_name=u'Имя',
		middle_name=u'Отчество',
		last_name=u'Фамилия',
	)
	form_columns = ('last_name', 'first_name', 'middle_name', 'email')
	form_args = dict(
		last_name=dict(label=gettext(u'Фамилия'), validators=[DataRequired(message = MSG_REQUIRED)]),
        first_name=dict(label=gettext(u'Имя'), validators=[DataRequired(message = MSG_REQUIRED)]),
        middle_name=dict(label=gettext(u'Отчество')),
        email=dict(label=u'Email', validators=[DataRequired(message = MSG_REQUIRED), Email(message = MSG_INVALID_EMAIL)]),
	)
	edit_modal = True
	create_modal = True
	can_set_page_size = True
	can_export = True
	can_delete = True

	def is_accessible(self):
		if not current_user.is_authenticated : return False
		if current_user.has_role('admin'): return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('login', next=request.url))

	def get_query(self):
		member_role = Role.query.get(ROLE_JUNADMIN)
		return (self.session.query(User).filter(User.roles.contains(member_role)))

	def get_count_query(self):
		junadmin_role = Role.query.get(ROLE_JUNADMIN)
		return (self.session.query(func.count('*')).filter(User.roles.contains(junadmin_role)))

	def create_model(self, form):
		try:
			model = self.model()
			form.populate_obj(model)
			jun_role = Role.query.get(ROLE_JUNADMIN)
			user = user_datastore.create_user(action = 'create', email = model.email,
		 		first_name = model.first_name,
		 		middle_name = model.middle_name,
		 		last_name = model.last_name,
		 		roles = [jun_role, ],
		 		money = 0,
		 		passive = False,
		 		active = True,
		 	)
			self._on_model_change(form, model, True)
			db.session.commit()
		except Exception as ex:
			if not self.handle_view_exception(ex):
				flash(u'Невозможно создать запись: %s' % str(ex), 'mycat')
			self.session.rollback()
			return False
		return model

	def update_model(self, form, model):
	 	try:
	 		form.populate_obj(model)
		 	checkStatus(model)
	 		self._on_model_change(form, model, False)
	 		self.session.commit()
	 	except Exception as ex:
	 		if not self.handle_view_exception(ex):
	 			flash(u'Невозможно редактировать запись: %s' % str(ex), 'mycat')
	 		self.session.rollback()
	 		return False
	 	else:
	 		self.after_model_change(form, model, False)
	 	return True

class TransactionAdminView(sqla.ModelView):
	@action('finish', u'Завершить')
	def action_finish(self, ids):
		for id in ids:
			tran = Transaction.query.get(id)
			if tran.status != u'Закрыто':
				tran.status = u'Закрыто'
				if tran.type == u'Куратору' and tran.to_user is not None:
					tran.to_user.money = tran.to_user.money + tran.sum
					checkStatus(tran.to_user)
		db.session.commit()
	column_list = ('from_user', 'sum', 'type', 'created', 'status', 'ex')
	column_labels = dict(
		from_user   = u'Пользователь',
		sum    = u'Сумма',
		type   = u'Направление',
		status = u'Статус',
		created = u'Создан',
		ex 		= u'Счет',
	)
	can_create = False
	can_edit = False
	column_formatters = dict(sum = lambda v, c, m, p: (str)(m.sum)+u' руб.')
	can_set_page_size = True
	column_searchable_list = ['from_user.email']
	column_filters = ['status','type']

	can_delete = True
	can_export = True

	def get_query(self):
		return (self.session.query(Transaction).order_by(Transaction.status.desc()))

	def is_accessible(self):
		if not current_user.is_authenticated: return False
		if current_user.has_role('admin'):	return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('security.login', next=request.url))

class WithdrawTicketAdminView(sqla.ModelView):
	@action('finish', u'Завершить')
	def action_finish(self, ids):
		for id in ids:
			ticket = WithdrawTicket.query.get(id)
			ticket.status = u'Закрыто'
			ticket.user.money = ticket.user.money - ticket.sum
			tran = Transaction(from_user = ticket.user, sum = ticket.sum, type=u'Пользователю', ex = ticket.account)
			db.session.add(ticket)
			db.session.add(tran)
		db.session.commit()
	column_list = ('user', 'sum', 'account', 'status', 'created')
	column_labels = dict(
		user   = u'Пользователь',
		sum    = u'Сумма',
		account = u'Счет',
		status = u'Статус',
		created = u'Создан'
	)
	can_create = False
	can_edit = False
	column_formatters = dict(sum = lambda v, c, m, p: (str)(m.sum)+u' руб.')
	can_set_page_size = True
	column_searchable_list = ['user.email']
	column_filters = ['status']

	can_delete = True
	can_export = True

	def get_query(self):
		return (self.session.query(WithdrawTicket).order_by('status'))

	def is_accessible(self):
		if not current_user.is_authenticated: return False
		if current_user.has_role('admin'):	return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('security.login', next=request.url))

# Panel Views For junadmin

class MemberJunAdminView(sqla.ModelView):
	column_searchable_list = ['first_name', 'email', 'last_name']
	column_filters = ['passive']
	column_labels = dict(
		first_name=u'Имя',
		middle_name=u'Отчество',
		last_name=u'Фамилия',
		curator=u'Куратор',
		money=u'Средства',
		passive=u'Пассивный',
		confirmed_at=u'Создан',
		full = u'Заполнен',
	)
	edit_modal = True
	create_modal = True
	form_args = dict(
		last_name=dict(label=gettext(u'Фамилия'), validators=[DataRequired(message = MSG_REQUIRED)]),
        first_name=dict(label=gettext(u'Имя'), validators=[DataRequired(message = MSG_REQUIRED)]),
        middle_name=dict(label=gettext(u'Отчество')),
        email=dict(label=u'Email', validators=[DataRequired(message = MSG_REQUIRED), Email(message = MSG_INVALID_EMAIL)]),
        money=dict(label=u'Средства'),
    )
	can_set_page_size = True

	column_formatters = dict(
		money = lambda v, c, m, p: (str)(m.money)+u' руб.',
		roles = lambda v, c, m, p: role_format(m.roles),
	)
	column_export_exclude_list = ('passive')
	column_list = ('last_name', 'first_name', 'middle_name', 'email', 'money', 'passive', 'confirmed_at', 'curator', 'full')
	form_columns = ('email', 'last_name', 'first_name','middle_name', 'money')
	can_delete = False

	def is_accessible(self):
		if not current_user.is_authenticated : return False
		if current_user.has_role('junadmin'): return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('login', next=request.url))

	def get_query(self):
		member_role = Role.query.get(ROLE_MEMBER)
		return (self.session.query(User).filter(User.roles.contains(member_role)))

	def get_count_query(self):
		member_role = Role.query.get(ROLE_MEMBER)
		return (self.session.query(func.count('*')).filter(User.roles.contains(member_role)))

	def create_model(self, form):
		try:
			model = self.model()
			form.populate_obj(model)
			role = Role.query.get(ROLE_MEMBER)
			model.roles = [role, ]
			user = user_datastore.create_user(action = 'create', email = model.email,
		 		first_name = model.first_name,
		 		middle_name = model.middle_name,
		 		last_name = model.last_name,
		 		curator = getPosition(),
		 		roles = model.roles,
		 		money = 0,
		 		passive = False,
		 		active = True,
		 	)
			self._on_model_change(form, model, True)
			user_datastore.commit()
			FreeSpaceCheck()
			total = model.money
			if total > 0:
			 	tran1 = Transaction(from_user = user, to_user = user.curator, type=u'Куратору', sum = getPercents(sum = total, type = 'curator'))
			 	tran2 = Transaction(from_user = user, type=u'Администратору', sum = getPercents(sum = total, type='admin'))
			 	tran3 = Transaction(from_user = user, type=u'Владельцу', sum = getPercents(sum=total, type='owner'))
			 	db.session.add(tran1)
			 	db.session.add(tran2)
			 	db.session.add(tran3)
			db.session.commit()
		except Exception as ex:
			if not self.handle_view_exception(ex):
				flash(u'Невозможно создать запись: %s' % str(ex), 'mycat')
			self.session.rollback()
			return False
		return model

	def update_model(self, form, model):
	 	try:
	 		self._on_model_change(form, model, False)
	 		form.populate_obj(model)
		 	checkStatus(model)
	 		self.session.commit()
	 	except Exception as ex:
	 		if not self.handle_view_exception(ex):
	 			flash(u'Невозможно редактировать запись: %s' % str(ex), 'mycat')
	 		self.session.rollback()
	 		return False
	 	else:
	 		self.after_model_change(form, model, False)
	 	return True

	def _on_model_change(self, form, model, is_created):
		if form.data['money'] > model.money and not is_created:
			diff = float(form.data['money']) - float(model.money)
			tran1 = Transaction(from_user = model, to_user = user.curator, type=u'Куратору', sum = getPercents(sum = diff, type = 'curator'))
			tran2 = Transaction(from_user = model, type=u'Администратору', sum = getPercents(sum = diff, type='admin'))
			tran3 = Transaction(from_user = model, type=u'Владельцу', sum = getPercents(sum = diff, type='owner'))
			db.session.add(tran1)
			db.session.add(tran2)
			db.session.add(tran3)
		elif not is_created and form.data['money'] < model.money:
			tran1 = Transaction.query.filter(Transaction.from_user == model).filter(Transaction.type == u'Куратору').filter(Transaction.status == u'Ожидает').order_by(Transaction.id.desc()).first()
			tran2 = Transaction.query.filter(Transaction.from_user == model).filter(Transaction.type == u'Администратору').filter(Transaction.status == u'Ожидает').order_by(Transaction.id.desc()).first()
			tran3 = Transaction.query.filter(Transaction.from_user == model).filter(Transaction.type == u'Владельцу').filter(Transaction.status == u'Ожидает').order_by(Transaction.id.desc()).first()
			tran1.sum = getPercents(sum = form.data['money'], type = 'curator')
			tran2.sum = getPercents(sum = form.data['money'], type='admin')
			tran3.sum = getPercents(sum = form.data['money'], type='owner')

class TransactionJunAdminView(sqla.ModelView):
	column_list = ('from_user', 'sum', 'type', 'created', 'status', 'ex')
	column_labels = dict(
		from_user   = u'Пользователь',
		sum    = u'Сумма',
		type   = u'Направление',
		status = u'Статус',
		created = u'Создан',
		ex 		= u'Дополнительно',
	)
	can_create = False
	can_edit = False
	can_delete = False
	column_formatters = dict(sum = lambda v, c, m, p: (str)(m.sum)+u' руб.')
	can_set_page_size = True
	column_searchable_list = ['from_user.email']
	column_filters = ['status','type']

	def get_query(self):
		return (self.session.query(Transaction).order_by('status'))

	def is_accessible(self):
		if not current_user.is_authenticated: return False
		if current_user.has_role('junadmin'): return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('login', next=request.url))

class WithdrawTicketJunAdminView(sqla.ModelView):
	column_list = ('user', 'sum', 'account', 'status')
	column_labels = dict(
		user   = u'Пользователь',
		sum    = u'Сумма',
		status = u'Статус',
		account 		= u'Дополнительно',
	)
	can_create = False
	can_edit = False
	can_delete = False
	column_formatters = dict(sum = lambda v, c, m, p: (str)(m.sum)+u' руб.')
	can_set_page_size = True
	column_searchable_list = ['user.email']
	column_filters = ['status']

	def get_query(self):
		return (self.session.query(WithdrawTicket).order_by('status'))

	def is_accessible(self):
		if not current_user.is_authenticated: return False
		if current_user.has_role('junadmin'): return True
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('login', next=request.url))



with app.app_context():
	# Adding panel views for admin
	admin.add_view(EmployerAdminView(User, db.session, name = u'Сотрудники', url = '/employees'))
	admin.add_view(MemberAdminView(User, db.session, name = u'Участники', url = '/amembers', endpoint = 'members'))
	admin.add_view(TransactionAdminView(Transaction, db.session, name = u'Переводы', url = '/transactions', endpoint = 'transactions'))
	admin.add_view(WithdrawTicketAdminView(WithdrawTicket, db.session, name = u'Заявки на вывод', url='/awdtickets', endpoint = 'awdtickets'))

	# Adding panel views for junadmin
	admin.add_view(MemberJunAdminView(User, db.session, name = u'Участники', url = '/jmembers', endpoint = 'members_for_junadmin'))
	admin.add_view(TransactionJunAdminView(Transaction, db.session, name = u'Переводы', url = '/transaactions', endpoint = 'transactions_for_junadmin'))
	admin.add_view(WithdrawTicketJunAdminView(WithdrawTicket, db.session, name = u'Заявки на вывод', url='/jwdtickets', endpoint = 'jwdtickets'))

@security.context_processor
def security_context_processor():
	return dict(
		admin_base_template = admin.base_template,
		admin_view = admin.index_view,
		h = admin_helpers,
		get_url = url_for
	)